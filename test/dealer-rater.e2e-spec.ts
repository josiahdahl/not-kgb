import { Test, TestingModule } from '@nestjs/testing';
import { AppModule } from '../src/app.module';
import { INestApplication } from '@nestjs/common';
import { RiskService } from '../src/risk/risk.service';
import { finalize } from 'rxjs';

describe('AppController (e2e)', () => {
  let app: INestApplication;

  beforeEach(async () => {
    const moduleFixture: TestingModule = await Test.createTestingModule({
      imports: [AppModule],
    }).compile();

    app = moduleFixture.createNestApplication();
    await app.init();
  });

  it(
    'fetches DealerRater reviews',
    (done) => {
      app
        .get<RiskService>(RiskService)
        .getDealerRaterReviews()
        .pipe(
          finalize(() => {
            app.close().then(() => {
              done();
            });
          }),
        )
        .subscribe((results) => {
          expect(results.length).toEqual(3);
          expect(results[0].sentiment).toBeGreaterThanOrEqual(
            results[1].sentiment,
          );
          expect(results[1].sentiment).toBeGreaterThanOrEqual(
            results[2].sentiment,
          );
        });
    },
    50 * 1000,
  );
});
