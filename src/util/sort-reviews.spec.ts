import { sortBySentiment } from './sort-by-sentiment';
import { ProcessedReview } from '../scraper/models';

describe('sortReviews', () => {
  function assertSorts(input, expected) {
    input.sort(sortBySentiment);
    expect(input).toEqual(expected);
  }
  it('handles undefined reviews', () => {
    assertSorts(
      [undefined, undefined, undefined],
      [undefined, undefined, undefined],
    );
  });

  it('handles undefined first review', () => {
    const review = ProcessedReview.parse({
      date: '',
      summary: '',
      username: '',
      text: '',
      sentiment: 5,
    });
    assertSorts([undefined, review], [review, undefined]);
  });

  it('handles undefined second review', () => {
    const review = ProcessedReview.parse({
      date: '',
      summary: '',
      username: '',
      text: '',
      sentiment: 5,
    });
    assertSorts([review, undefined], [review, undefined]);
  });

  it('sorts reviews', () => {
    const review = ProcessedReview.parse({
      date: '',
      summary: '',
      username: '',
      text: '',
      sentiment: 5,
    });

    const review2 = ProcessedReview.parse({
      date: '',
      summary: '',
      username: '',
      text: '',
      sentiment: 8,
    });
    assertSorts([review, review2], [review2, review]);
    assertSorts([review2, review], [review2, review]);
  });
});
