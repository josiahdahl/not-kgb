import { ProcessedReview } from '../scraper/models';

/**
 * Sort reviews by sentiment
 */
export function sortBySentiment(
  r1: ProcessedReview | undefined,
  r2: ProcessedReview | undefined,
) {
  if (!r1 && !r2) {
    return 0;
  }
  if (!r1) {
    return 1;
  }
  if (!r2) {
    return -1;
  }
  return r1.sentiment > r2.sentiment ? -1 : 1;
}
