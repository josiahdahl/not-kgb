import { Logger, Module } from '@nestjs/common';
import { LoggerModule } from 'nestjs-pino';
import { ConfigModule } from './config/config.module';
import { ScraperModule } from './scraper/scraper.module';
import { SentimentModule } from './sentiment/sentiment.module';
import { RiskModule } from './risk/risk.module';
import { ConfigService } from './config/config.service';

@Module({
  imports: [
    LoggerModule.forRootAsync({
      useFactory: (configService: ConfigService) => {
        setInterval(() => {
          // Must flush logger manually due to CLI usage
          Logger.flush();
        }, 100);
        return {
          pinoHttp: {
            level: configService.get('logLevel'),
          },
        };
      },
      inject: [ConfigService],
      imports: [ConfigModule],
    }),
    ConfigModule,
    ScraperModule,
    SentimentModule,
    RiskModule,
  ],
})
export class AppModule {}
