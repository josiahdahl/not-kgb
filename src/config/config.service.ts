import { Injectable } from '@nestjs/common';
import { Env } from './environment.interface';

@Injectable()
export class ConfigService {
  private readonly env: Env;
  constructor() {
    this.env = Env.parse({
      sentimentVocabulary: process.env.SENTIMENT_VOCABULARY,
      dealerRaterStarValue: process.env.DEALER_RATER_STAR_VALUE
        ? Number(process.env.DEALER_RATER_STAR_VALUE)
        : undefined,
      dealerRaterRecommendValue: process.env.DEALER_RATER_RECOMMEND_VALUE
        ? Number(process.env.DEALER_RATER_RECOMMEND_VALUE)
        : undefined,
      logLevel: process.env.LOG_LEVEL || 'info',
    });
  }

  /**
   * Get a config value by key
   */
  get<T extends keyof Env>(key: T): Env[T] {
    return this.env[key];
  }
}
