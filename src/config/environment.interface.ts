import { z } from 'zod';

export const Env = z.object({
  sentimentVocabulary: z.string().default('afinn'),
  dealerRaterStarValue: z.number().default(0.5),
  dealerRaterRecommendValue: z.number().default(1),
  logLevel: z.enum(['info', 'debug', 'warn', 'error', 'verbose', 'silent']),
});

export type Env = z.infer<typeof Env>;
