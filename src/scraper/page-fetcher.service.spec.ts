import { PageFetcherService, UrlFormatter } from './page-fetcher.service';
import { Test, TestingModule } from '@nestjs/testing';
import { HttpModule, HttpService } from '@nestjs/axios';
import { AxiosResponse } from 'axios';
import { of, throwError, toArray } from 'rxjs';

describe('PageFetcherService', () => {
  let service: PageFetcherService;
  let http$: HttpService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [PageFetcherService],
      imports: [HttpModule],
    }).compile();

    service = module.get<PageFetcherService>(PageFetcherService);
    http$ = module.get<HttpService>(HttpService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });

  describe('fetchRawPages', () => {
    it('fetches the desired number of pages with the correct url', (done) => {
      jest.spyOn(service, 'fetchRawPage');
      jest.spyOn(http$, 'get').mockImplementation((url) =>
        of({
          data: `Mock axios response for ${url}`,
        } as AxiosResponse),
      );

      const formatter: UrlFormatter = (page: number) => `Page ${page}`;

      service
        .fetchRawPages(5, formatter, 5)
        .pipe(toArray())
        .subscribe((results) => {
          expect(results.length).toEqual(5);
          for (let i = 0; i < results.length; i++) {
            expect(results[i].isOk()).toBeTruthy();
            expect(results[i].unwrapOr('')).toEqual(
              `Mock axios response for ${formatter(i + 1)}`,
            );
          }
          done();
        });
    });
  });

  describe('fetchRawPage', () => {
    it('handles non 2xx responses', (done) => {
      jest.spyOn(http$, 'get').mockImplementation((_) =>
        throwError(() => ({
          response: {
            isAxiosError: true,
            status: 404,
          },
        })),
      );
      service.fetchRawPage('broken').subscribe((res) => {
        expect(res.isErr()).toBeTruthy();
        res.mapErr((err) => {
          expect(err).toEqual('Failed to fetch broken with status 404');
        });
        done();
      });
    });
  });
});
