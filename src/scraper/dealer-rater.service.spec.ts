import { Test, TestingModule } from '@nestjs/testing';
import { DealerRaterService } from './dealer-rater.service';
import { PageFetcherService } from './page-fetcher.service';
import { cheerioProvider } from './providers';
import { HttpModule } from '@nestjs/axios';
import { readFile } from 'fs/promises';
import * as path from 'path';
import { of } from 'rxjs';
import { ok } from 'neverthrow';
import { SentimentModule } from '../sentiment/sentiment.module';
import { ConfigService } from '../config/config.service';

describe('DealerRaterService', () => {
  let reviewHtml: string;
  let service: DealerRaterService;
  let fetcher: PageFetcherService;

  beforeAll(async () => {
    reviewHtml = (
      await readFile(
        path.join(
          __dirname,
          './test-data/review-page-missing-second-review.html',
        ),
      )
    ).toString();
  });

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        DealerRaterService,
        PageFetcherService,
        cheerioProvider,
        ConfigService,
      ],
      imports: [HttpModule, SentimentModule],
    }).compile();

    service = module.get<DealerRaterService>(DealerRaterService);
    fetcher = module.get<PageFetcherService>(PageFetcherService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });

  describe('urlFormatter', () => {
    it('contains the correct page numbers', () => {
      const one = DealerRaterService.urlFormatter('someBase', 'someDealer')(1);
      const two = DealerRaterService.urlFormatter(
        'someBase2',
        'someDealer5',
      )(2);

      expect(one).not.toContain('page1');
      expect(one).toContain('someBase/someDealer');
      expect(two).toContain('page2');
      expect(two).toContain('someBase2/someDealer5');
    });
  });

  describe('extractReviews', () => {
    it('extracts reviews from the page', () => {
      const reviews = service.extractReviews(reviewHtml);
      expect(reviews.length).toEqual(9);
      const first = reviews[0];

      expect(first.date).toEqual('September 13, 2021');
      expect(first.summary).toEqual(
        'McKaig Chevrolet gave us a pleasant buying experience',
      );
      expect(first.username).toEqual('candicerenee81');
      expect(first.text).toEqual(
        'McKaig Chevrolet gave us a pleasant buying experience. Such friendly, knowledgeable staff that were a pleasure to work with.',
      );

      const second = reviews[5]; // This review has content below the fold
      expect(second.date).toEqual('August 18, 2021');
      expect(second.summary).toEqual(
        'My excellent experience with McKaig started with a very',
      );
      expect(second.username).toEqual('Steve W.');
      // Remove whitespace because the HTML has multiple spaces in several places. We only care about the text.
      expect(second.text.replace(/\s/g, '')).toEqual(
        "My excellent experience with McKaig started with a very helpful phone conversation with Summer at reception, and then from the moment I walked into the dealership, I was met with very courteous and competent assistance.  Dennis listened carefully to discern my needs, took me for a test drive, and expertly handled all of my questions. He was the key player in closing the deal. Brandon and Taylor were very efficient in taking care of the rest of the paperwork and I was out the door with my new car in the same day! I couldn't be more pleased. Thank you!! Steve W.".replace(
          /\s/g,
          '',
        ),
      );
    });

    it('handles missing summaries when parsing reviews', () => {
      const reviewHtml =
        '<div id="reviews"><div class="review-entry"><!-- Why would a review be empty?? --></div></div>';
      const result = service.extractReviews(reviewHtml);
      console.log(result);
      expect(result.length).toEqual(0);
    });
  });

  describe('scrape', () => {
    it('fetches reviews and calculates sentiment', (done) => {
      jest
        .spyOn(fetcher, 'fetchRawPages')
        .mockImplementation(() => of(ok<string, string>(reviewHtml)));

      service.scrape().subscribe((reviews) => {
        expect(reviews.length).toEqual(9);
        done();
      });
    });
  });
});
