import { Inject, Injectable, Logger } from '@nestjs/common';
import { Scraper } from './scraper.interface';
import { DealerRaterReview, ProcessedReview } from './models';
import { filter, map, Observable } from 'rxjs';
import { CHEERIO, Cheerio } from './providers';
import { PageFetcherService } from './page-fetcher.service';
import { SentimentService } from '../sentiment/sentiment.service';
import { ConfigService } from '../config/config.service';

@Injectable()
export class DealerRaterService implements Scraper {
  private logger = new Logger(DealerRaterService.name);
  private baseUrl = 'https://www.dealerrater.com/dealer';
  private pages = 5;

  /**
   * Create a formatter for the dealership review page. The "ONLY_POSITIVE" filter is used
   * to ensure we only see positive reviews, maximizing our chance at weeding
   * out hyper positive reviews.
   */
  static urlFormatter(baseUrl: string, dealerSlug: string) {
    return (pageNum: number) => {
      return `${baseUrl}/${dealerSlug}/${
        pageNum === 1 ? '' : `page${pageNum}/`
      }?filter=ONLY_POSITIVE`;
    };
  }

  constructor(
    private readonly fetcher: PageFetcherService,
    @Inject(CHEERIO) private readonly cheerio: Cheerio,
    private readonly sentimentService: SentimentService,
    private readonly configService: ConfigService,
  ) {}

  /**
   * Get the first five pages of reviews from DealerRater.com for McKaig Chevrolet Buick
   *
   * FUTURE: scrape could take a specific dealer slug to scrape
   */
  scrape(): Observable<ProcessedReview[]> {
    return this.fetcher
      .fetchRawPages(
        this.pages,
        DealerRaterService.urlFormatter(
          this.baseUrl,
          'McKaig-Chevrolet-Buick-A-Dealer-For-The-People-dealer-reviews-23685',
        ),
      )
      .pipe(
        filter((result) => result.isOk()),
        map((result) => this.extractReviews(result.unwrapOr(''))),
        map((reviews) => reviews.map((r) => this.calculateSentiment(r))),
      );
  }

  calculateSentiment(review: DealerRaterReview) {
    const sentiment = this.sentimentService.analyze(review.text);
    return ProcessedReview.parse({
      ...review,
      sentiment: sentiment + review.sentimentModifier,
    });
  }

  extractReviews(rawHtml: string): DealerRaterReview[] {
    const multiWhitespace = /\s+/g;
    const $ = this.cheerio.load(rawHtml);
    const reviews: DealerRaterReview[] = [];

    $('#reviews .review-entry').each((i, el) => {
      try {
        const $el = $(el);
        const dateEl = $el.find('.review-date .font-20').first();
        const summaryEl = $el.find('.review-wrapper h3.no-format').first();
        const summary = !summaryEl.text()
          ? ''
          : summaryEl
              .text()
              .trim()
              .replace(/^"/, '')
              .replace(/\.+"$/, '')
              .replace(multiWhitespace, ' ');
        const username = summaryEl.next()?.text();
        const textEl = $el.find('.review-wrapper .review-content').first();

        const starContainer = $el.find(
          '.review-ratings-all .rating-static-indv',
        );
        const starMultiplier = this.configService.get('dealerRaterStarValue');
        const oneStar = starContainer.find('rating-10').toArray();
        const twoStar = starContainer.find('rating-20').toArray();
        const threeStar = starContainer.find('rating-30').toArray();
        const fourStar = starContainer.find('rating-40').toArray();
        const fiveStar = starContainer.find('rating-50').toArray();
        const starValue =
          oneStar.length * -2 * starMultiplier +
          twoStar.length * -1 * starMultiplier +
          threeStar.length * 0.5 * starMultiplier +
          fourStar.length * starMultiplier +
          fiveStar.length * 2 * starMultiplier;

        const recommendModifier = this.configService.get(
          'dealerRaterRecommendValue',
        );
        const userRecommends = $el
          .find('.review-rations-all .td.small-text.boldest')
          .first()
          .text()
          .includes('Yes');

        const parsedReview = DealerRaterReview.parse({
          date: dateEl?.text().replace(multiWhitespace, ' ') ?? '',
          summary,
          username: !username ? '' : username.replace(/^[-\s]+/g, ''),
          text: !textEl.text()
            ? ''
            : textEl.text().trim().replace(multiWhitespace, ' '),
          sentimentModifier:
            starValue +
            (userRecommends ? recommendModifier : -1 * recommendModifier),
        });
        if (parsedReview.summary) {
          reviews.push(parsedReview);
        }
      } catch (e) {
        this.logger.error(`Unable to extract review data from entry ${i}`, e);
      }
    });

    return reviews;
  }
}
