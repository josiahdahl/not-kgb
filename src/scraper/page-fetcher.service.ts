import { Injectable, Logger } from '@nestjs/common';
import { HttpService } from '@nestjs/axios';
import { catchError, map, mergeMap, Observable, of, range } from 'rxjs';
import { err, ok, Result } from 'neverthrow';

export type UrlFormatter = (page: number) => string;

@Injectable()
export class PageFetcherService {
  private readonly logger = new Logger(PageFetcherService.name);
  constructor(private readonly http$: HttpService) {}

  /**
   * Fetch the HTML as a string for the given number of pages. Provide a formatter function that
   * builds the URL based on the current page.
   */
  fetchRawPages(
    pageCount: number,
    urlFormatter: UrlFormatter,
    concurrentRequests = 5,
  ): Observable<Result<string, string>> {
    return range(1, pageCount).pipe(
      mergeMap(
        (page) => this.fetchRawPage(urlFormatter(page)),
        concurrentRequests,
      ),
    );
  }

  fetchRawPage(url: string): Observable<Result<string, string>> {
    this.logger.debug(`fetching ${url}`);
    return this.http$.get<string>(url).pipe(
      map((res) => ok<string, string>(res.data)),
      catchError((error) => {
        if (
          typeof error?.response.isAxiosError !== 'undefined' &&
          error.response.isAxiosError
        ) {
          this.logger.error(error.response);
          return of(
            err<string, string>(
              `Failed to fetch ${url} with status ${error.response.status}`,
            ),
          );
        }
        if (error.result) {
          this.logger.error(error.result);
        } else {
          this.logger.error(`Unknown error fetching ${url}`);
        }
        return of(err<string, string>(`Failed to fetch ${url}`));
      }),
    );
  }
}
