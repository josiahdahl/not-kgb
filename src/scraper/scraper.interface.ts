import { ProcessedReview } from './models';
import { Observable } from 'rxjs';

export interface Scraper {
  scrape(): Observable<ProcessedReview[]>;
}
