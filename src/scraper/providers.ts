import * as cheerio from 'cheerio';
export const CHEERIO = Symbol('CHEERIO_TOKEN');

export type Cheerio = typeof cheerio;

export const cheerioProvider = {
  provide: CHEERIO,
  useValue: cheerio,
};
