import { z } from 'zod';

export const Review = z.object({
  date: z.string(),
  summary: z.string(),
  username: z.string(),
  text: z.string(),
});

export type Review = z.infer<typeof Review>;

export const ProcessedReview = Review.extend({
  sentiment: z.number(),
});

export type ProcessedReview = z.infer<typeof ProcessedReview>;

export const DealerRaterReview = Review.extend({
  sentimentModifier: z.number(),
});

export type DealerRaterReview = z.infer<typeof DealerRaterReview>;
