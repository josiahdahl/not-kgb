import { Module } from '@nestjs/common';
import { DealerRaterService } from './dealer-rater.service';
import { HttpModule } from '@nestjs/axios';
import { cheerioProvider } from './providers';
import { SentimentModule } from '../sentiment/sentiment.module';
import { PageFetcherService } from './page-fetcher.service';
import { ConfigModule } from '../config/config.module';

@Module({
  providers: [DealerRaterService, cheerioProvider, PageFetcherService],
  imports: [HttpModule, SentimentModule, ConfigModule],
  exports: [DealerRaterService],
})
export class ScraperModule {}
