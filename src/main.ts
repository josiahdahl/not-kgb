import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { Logger } from 'nestjs-pino';
import { config } from 'dotenv';
import { finalize } from 'rxjs';
import { RiskService } from './risk/risk.service';

config();

async function bootstrap() {
  try {
    const app = await NestFactory.create(AppModule, {
      bufferLogs: true,
      autoFlushLogs: false,
    });
    app.useLogger(app.get(Logger));

    app
      .get<RiskService>(RiskService)
      .getDealerRaterReviews()
      .pipe(
        finalize(() => {
          process.exit(0);
        }),
      )
      .subscribe((results) => {
        console.log('DealerRater.com Results');
        console.log(results);
      });
  } catch (e) {
    console.error(e);
    process.exit(1);
  }
}
bootstrap();
