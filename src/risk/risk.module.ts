import { Module } from '@nestjs/common';
import { ConfigModule } from '../config/config.module';
import { RiskService } from './risk.service';
import { ScraperModule } from '../scraper/scraper.module';

@Module({
  imports: [ConfigModule, ScraperModule],
  providers: [RiskService],
  exports: [RiskService],
})
export class RiskModule {}
