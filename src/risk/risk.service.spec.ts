import { RiskService } from './risk.service';
import { Test, TestingModule } from '@nestjs/testing';
import { DealerRaterService } from '../scraper/dealer-rater.service';
import { Scraper } from '../scraper/scraper.interface';
import { ProcessedReview } from '../scraper/models';
import { from, Observable, of } from 'rxjs';
import { sortBySentiment } from '../util/sort-by-sentiment';

function fakeReview(sentiment: number = Math.random()) {
  return ProcessedReview.parse({
    sentiment,
    date: '',
    summary: '',
    username: '',
    text: '',
  });
}

class MockScraper implements Scraper {
  scrape(): Observable<ProcessedReview[]> {
    return of([]);
  }
}

describe('RiskService', () => {
  let service: RiskService;
  let mockScraper: MockScraper;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        RiskService,
        { provide: DealerRaterService, useClass: MockScraper },
      ],
    }).compile();

    service = module.get<RiskService>(RiskService);
    mockScraper = module.get<MockScraper>(DealerRaterService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });

  describe('getDealerRaterReviews', () => {
    it('scrapes reviews and picks the top three', (done) => {
      const pageReviews = [
        [fakeReview(), fakeReview(), fakeReview()],
        [fakeReview(), fakeReview(), fakeReview()],
        [fakeReview(), fakeReview(), fakeReview()],
      ];

      const expectedTop3 = pageReviews.flat().sort(sortBySentiment).slice(0, 3);

      jest
        .spyOn(mockScraper, 'scrape')
        .mockImplementation(() => from(pageReviews));

      service.getDealerRaterReviews().subscribe((reviews) => {
        expect(reviews).toEqual(expectedTop3);
        done();
      });
    });
  });
});
