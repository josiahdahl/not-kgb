import { Injectable, Logger } from '@nestjs/common';
import { DealerRaterService } from '../scraper/dealer-rater.service';
import { reduce } from 'rxjs';
import { ProcessedReview } from '../scraper/models';
import { sortBySentiment } from '../util/sort-by-sentiment';

type TopThreeReviews = [
  ProcessedReview | undefined,
  ProcessedReview | undefined,
  ProcessedReview | undefined,
];

@Injectable()
export class RiskService {
  private readonly logger = new Logger(RiskService.name);
  constructor(private readonly dealerRaterService: DealerRaterService) {}

  /**
   * Keep track of the top three reviews, ordered by sentiment desc.
   * @future could this be refactored to take the top n reviews?
   */
  private static pickTopThree(
    [one, two, three]: [ProcessedReview, ProcessedReview, ProcessedReview],
    review: ProcessedReview,
  ) {
    if (typeof one === 'undefined') {
      return [review, undefined, undefined];
    }

    if (typeof two === 'undefined') {
      return [one, review, undefined].sort(sortBySentiment);
    }

    if (typeof three === 'undefined') {
      return [one, two, review].sort(sortBySentiment);
    }

    if (review.sentiment > one.sentiment) {
      return [review, one, two];
    }

    if (review.sentiment > two.sentiment) {
      return [one, review, two];
    }

    if (review.sentiment > three.sentiment) {
      return [one, two, review];
    }

    return [one, two, three];
  }

  /**
   * Get the top three reviews from DealerRater.com
   */
  getDealerRaterReviews() {
    this.logger.log('Assessing risk of DealerRater reviews');
    const seed: TopThreeReviews = [undefined, undefined, undefined];
    return this.dealerRaterService.scrape().pipe(
      reduce((topThree, reviews) => {
        return reviews.reduce(RiskService.pickTopThree, topThree);
      }, seed),
    );
  }
}
