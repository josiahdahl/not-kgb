import { SentimentAnalyzer, PorterStemmer, SentenceTokenizer } from 'natural';
import { ConfigService } from '../config/config.service';
export const ENGLISH_SENTIMENT_ANALYZER = Symbol('ENGLISH_SENTIMENT_ANALYZER');
export const englishSentimentProvider = {
  provide: ENGLISH_SENTIMENT_ANALYZER,
  useFactory: (configService: ConfigService) => {
    return new SentimentAnalyzer(
      'English',
      PorterStemmer,
      configService.get('sentimentVocabulary'),
    );
  },
  inject: [ConfigService],
};

export const sentenceTokenizerProvider = {
  provide: SentenceTokenizer,
  useClass: SentenceTokenizer,
};
