import { Module } from '@nestjs/common';
import { SentimentService } from './sentiment.service';
import {
  englishSentimentProvider,
  sentenceTokenizerProvider,
} from './providers';
import { ConfigModule } from '../config/config.module';

@Module({
  imports: [ConfigModule],
  providers: [
    SentimentService,
    englishSentimentProvider,
    sentenceTokenizerProvider,
  ],
  exports: [SentimentService],
})
export class SentimentModule {}
