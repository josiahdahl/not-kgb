import { Inject, Injectable } from '@nestjs/common';
import { ENGLISH_SENTIMENT_ANALYZER } from './providers';
import { SentenceTokenizer, SentimentAnalyzer } from 'natural';

@Injectable()
export class SentimentService {
  constructor(
    @Inject(ENGLISH_SENTIMENT_ANALYZER)
    private readonly sentimentAnalyzer: SentimentAnalyzer,
    private readonly sentenceTokenizer: SentenceTokenizer,
  ) {}

  /**
   * Analyze the sentiment of a section of text by splitting it up into sentences
   * then summing the sentiment of every sentence. Negative values are bad, positive
   * values are good
   */
  analyze(text: string) {
    const sentences = this.sentenceTokenizer.tokenize(text);
    return sentences.reduce((sentiment, sentence) => {
      return (
        sentiment + this.sentimentAnalyzer.getSentiment(sentence.split(' '))
      );
    }, 0);
  }
}
