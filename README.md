# Not KGB

Definitely not.

## Description

If you know, you know too much.

## Approach and Criteria to Review Analysis

The initial approach to sentiment analysis for this project was to rate each review using common elements of American English.
Metrics like the number of exclamation marks, usage of positive adjectives ("awesome", "great", "best"), or mentions of a specific employee
(looking at you, ~~товарищ~~ Adrian).

However, after doing some research, the npm package [natural](http://naturalnode.github.io/natural/) was selected
to analyze sentiment. The package supports several word databases which will provide a diverse range of ratings and help
determine the most positive reviews for "removal".

The application can be configured to use different word databases as well as weight elements of each review differently
to pinpoint reviews that need "adjustment".

See [Environment Variables](#Environment-Variables) for the defaults as well as valid values.

## Roadmap

- [x] Get pages and scrape content
- [x] Analyze content for sentiment with `natural`
- [x] Audit testing, ensure coverage
- [x] Configure log level
- [x] Package in Docker
- [x] GitHub actions
- [x] Documentation/README

### Future Enhancements

- [ ] Output optional performance statistics
  - [ ] Total run time
  - [ ] Time in each section (fetching, processing)
- [ ] Profile application
- [ ] Implement CLI option - pages
- [ ] Implement CLI option - number of reviews

## Development

### Installation

`not-kgb` requires Node 16. Other versions may work, but compatability is not guaranteed.

```bash
$ npm install
```

### Running the app locally

> Optional: Copy `.example.env` to `.env` and modify the settings to configure the app. Sensible defaults are provided.
> See [Environment Variables](#Environment-Variables) for more information.

```bash
# development
$ npm run start

# watch mode
$ npm run start:dev

# production mode
$ npm run build && npm run start:prod

# Run via Docker image
$ docker run --rm ghcr.io/josiahdahl/not-kgb:latest
# With environment variables
$ docker run --rm -e SENTIMENT_VOCABULARY=senticon -e DEALER_RATER_STAR_VALUE=.1 -e DEALER_RATER_RECOMMEND_VALUE=.3 ghcr.io/josiahdahl/not-kgb:latest
```

### Testing

```bash
# unit tests
$ npm run test

# e2e tests
$ npm run test:e2e

# test coverage
$ npm run test:cov
```

### Build the Docker image

```bash
$ docker build -t <your-tag-here> .
```

### Environment Variables

Copy `.example.env` to `.env` to modify the variables, or run the Docker image with none to many variables set to influence
the application's behavior.

`SENTIMENT_VOCABULARY` - Default `afinn`. Sentiment vocabularies supported by [natural](http://naturalnode.github.io/natural/).
one of `afinn`, `senticon`, or `pattern`

`DEALER_RATER_STAR_VALUE` - Default `0.5`. Influences the effect of each star on DealerRater.com reviews. Higher value will make
1 and 2 stars more negative and 3, 4, 5 stars more positive.

`DEALER_RATER_RECOMMEND_VALUE` - Default `1`. How many points "Recommend Dealer" is worth on DealerRater.com. Positive if Yes, negative if No.

`LOG_LEVEL` - Default `info`. Log levels supported by [pino-http](https://github.com/pinojs/pino-http).
One of `info`, `debug`, `warn`, `error`, `verbose`, `silent`

## CI/CD

GitHub Actions are configured to run on every pull request to `master` and on every push to `master`.
A release happens every time a new tag is pushed matching `v*`.

A release consists of building a Docker image and pushing it to the GitHub Container Registry with the names
`ghcr.io/josiahdahl/not-kgb:latest` and `ghcr.io/josiahdahl/not-kgb:<tag>`.

## Technology and Architecture

`not-kgb` uses [NestJS](https://nestjs.com/). Nest is a node.js framework for building server-side applications. While it generally
used to build applications that run an HTTP server, running an HTTP server is not required to use the framework and this project
does not expose or use a webserver. Nest was chosen for its modular architecture, IoC container, rich ecosystem, and TypeScript support.

The functionality in `not-kgb` is separated into three main modules, `risk`, `scraper`, and `sentiment`. Each module is responsible for one
aspect of scraping the reviews.

`scraper` exports a single service, `DealerRaterService`, that can be used to fetch the first five
pages of reviews for "the dealership" from DealerRater.com. Internally, `DealerRaterService` uses a URL agnostic
`PageFetcherService` that manages concurrently fetching pages for analysis. Once we expand our operations the `DealerRaterService`
can easily be enhanced to support many dealerships on DealerRater.com. If other sites need to be scraped in the future the
necessary service classes can be created that implement `Scraper` and they can reuse the `PageFetcherService`.

`sentiment` is responsible for analyzing the sentiment of arbitrary text. It exports a single service, `SentimentService`.
The service can be configured to use one of several sentiment databases.

`risk` is the entry point into the review analysis process. `RiskService` has methods for scraping each site and
picking the top three most problematic reviews for adjustment.

Na Zdorovie!
